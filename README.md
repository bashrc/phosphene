<img src="https://code.freedombone.net/bashrc/phosphene/raw/master/phosphene.png?raw=true" width=640/>

Phosphene is some C code which can be used to plot data in the style of a Cathode Ray Oscilloscope with a green phosphor screen.  This may be useful for visualising engineering data in a manner which has a similar aesthetic to the physical hardware.

This code is distribued under an ultra-permissive MIT license with the intention that it may be used within any project with minimal chances of license incompatibilities.

Example usage
-------------

``` C
scope s;
unsigned int channel = 0; /* channel 0 or 1 */
unsigned int t;
double voltage;
double min_voltage = 0;
double max_voltage = 25;
unsigned int time_step_ms = 1;
unsigned int intensity_percent = 100;

/* dimensions of the oscilloscope grid */
unsigned int grid_horizontal = 10;
unsigned int grid_vertical = 8;

/* image data */
unsigned int img_width = 640;
unsigned int img_height = 480;
unsigned char img[640*480*3];

/* create the oscilloscope */
create_scope(&s, time_step_ms);
s.offset_ms = 0; /* horizontal offset */
s.marker_position = 200; /* position of a marker on the horizontal axis */
s.time_ms = 1000; /* max time on the horizontal axis */

for (t = 0; t < 1000/time_step_ms; t++) {
    /* sinusoidal voltage in the range 0-24v */
    voltage = (1+sin(t*3.1415927/500.0))*max_voltage*0.5;

    scope_update(&s, channel, voltage,
                 min_voltage, max_voltage,
                 t/time_step_ms, 0);
}

/* create the oscilloscope image */
scope_draw(&s, PHOSPHENE_DRAW_ALL, intensity_percent,
           grid_horizontal, grid_vertical,
           (unsigned char*)img, img_width, img_height,
           PHOSPHENE_SHAPE_RECTANGULAR);

/* save the image to a file */
phosphene_write_png_file("scope1.png",
                         (int)img_width, (int)img_height, 24,
                         (unsigned char*)img);
```

Alternatively you can use the *scope_draw_graph* function to plot with axes and titles.

<img src="https://code.freedombone.net/bashrc/phosphene/raw/master/phosphene_graph.png?raw=true" width=640/>

``` C
scope s;
unsigned int channel = 0; /* channel 0 or 1 */
unsigned int t;
double voltage;
double min_voltage = 0;
double max_voltage = 25;
unsigned int time_step_ms = 1;
unsigned int intensity_percent = 100;

/* dimensions of the oscilloscope grid */
unsigned int grid_horizontal = 20;
unsigned int grid_vertical = 16;

/* image data */
unsigned int img_width = 640;
unsigned int img_height = 480;
unsigned char img[640*480*3];

/* create the oscilloscope */
create_scope(&s, time_step_ms);
s.offset_ms = 0; /* horizontal offset */
s.marker_position = 200; /* position of a marker on the horizontal axis */
s.time_ms = 1000; /* max time on the horizontal axis */

for (t = 0; t < 1000/time_step_ms; t++) {
    /* sinusoidal voltage in the range 0-24v */
    voltage = (1+sin(t*3.1415927/500.0))*max_voltage*0.5;

    scope_update(&s, channel, voltage,
                 min_voltage, max_voltage,
                 t/time_step_ms, 0);
}

/* create the oscilloscope image */
scope_draw_graph(&s, PHOSPHENE_DRAW_ALL, 3, 100,
                 grid_horizontal, grid_vertical,
                 (unsigned char*)img, img_width, img_height,
                 PHOSPHENE_SHAPE_RECTANGULAR,
                 "Graph Title",
                 "Vertical Values", "Horizontal Values", 15, 4);

/* save the image to a file */
phosphene_write_png_file("scope2.png",
                         (int)img_width, (int)img_height, 24,
                         (unsigned char*)img);
```
