/****************************************************************

 Phosphene: test program

 =============================================================

 Copyright 2013,2015 Bob Mottram
 bob@robotics.uk.to EA982E38

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the followingp
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

****************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../phosphene.h"

int main(int argc, char* argv[])
{
    scope s;
    unsigned int channel = 0; /* channel 0 or 1 */
    unsigned int i, t;
    double voltage;
    double min_voltage = 0;
    double max_voltage = 25;
    unsigned int time_step_ms = 1;
    unsigned int intensity_percent = 100;

    /* dimensions of the oscilloscope grid */
    unsigned int grid_horizontal = 10;
    unsigned int grid_vertical = 8;

    /* image data */
    unsigned int img_width = 640;
    unsigned int img_height = 480;
    unsigned char img[640*480*3];

    /* create the oscilloscope */
    create_scope(&s, time_step_ms);
    s.offset_ms = 0; /* horizontal offset */
    s.marker_position = 200; /* position of a marker on the horizontal axis */
    s.time_ms = 1000; /* max time on the horizontal axis */

    for (t = 0; t < 1000/time_step_ms; t++) {
        /* sinusoidal voltage in the range 0-24v */
        voltage = (1+sin(t*3.1415927/500.0))*max_voltage*0.5;

        scope_update(&s, channel, voltage,
                     min_voltage, max_voltage,
                     t/time_step_ms, 0);
    }

    /* create the oscilloscope image */
    scope_draw(&s, PHOSPHENE_DRAW_ALL, intensity_percent,
               grid_horizontal, grid_vertical,
               (unsigned char*)img, img_width, img_height,
               PHOSPHENE_SHAPE_RECTANGULAR);

    scope_text("Hello World", &s,
               img_width/4, img_height/10,
               30, 30, 75,
               (unsigned char*)img,
               img_width, img_height);

    scope_text_vertical("Vertical Text", &s,
                        img_width/4, img_height*8/10,
                        20, 20, 100,
                        (unsigned char*)img,
                        img_width, img_height);

    /* save the image to a file */
    if (phosphene_write_png_file("scope1.png",
                                 (int)img_width, (int)img_height, 24,
                                 (unsigned char*)img) != 0) {
        printf("Failed to save scope1.png\n");
        return -1;
    }
    printf("scope1.png saved\n");

    scope_draw_graph(&s, PHOSPHENE_DRAW_ALL, 3, 100,
                     grid_horizontal*2, grid_vertical*2,
                     (unsigned char*)img, img_width, img_height,
                     PHOSPHENE_SHAPE_RECTANGULAR,
                     "Graph Title",
                     "Vertical Values", "Horizontal Values", 15, 4);

    /* save the image to a file */
    if (phosphene_write_png_file("scope2.png",
                                 (int)img_width, (int)img_height, 24,
                                 (unsigned char*)img) != 0) {
        printf("Failed to save scope2.png\n");
        return -1;
    }
    printf("scope2.png saved\n");

    /* create the oscilloscope image */
    for (i = 0; i < img_width*img_height*3; i++) img[i] = 255;

    scope_draw_bounded(&s, PHOSPHENE_DRAW_ALL, intensity_percent,
                       grid_horizontal, grid_vertical,
                       (unsigned char*)img,
                       img_width*1/20, img_height*1/10,
                       img_width*14/20, img_height*14/20,
                       img_width, img_height,
                       PHOSPHENE_SHAPE_RECTANGULAR, 0);
    scope_draw_bounded(&s, PHOSPHENE_DRAW_ALL, intensity_percent,
                       grid_horizontal, grid_vertical,
                       (unsigned char*)img,
                       img_width*12/20, img_height*10/20,
                       img_width*19/20, img_height*19/20,
                       img_width, img_height,
                       PHOSPHENE_SHAPE_CIRCULAR, 0);

    /* save the image to a file */
    if (phosphene_write_png_file("scope3.png",
                                 (int)img_width, (int)img_height, 24,
                                 (unsigned char*)img) != 0) {
        printf("Failed to save scope3.png\n");
    }
    printf("scope3.png saved\n");
    printf("Exit success\n");

    return 0;
}
